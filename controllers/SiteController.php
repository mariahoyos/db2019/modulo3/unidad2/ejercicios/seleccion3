<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Ciclista;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\Lleva;
use app\models\Puerto;
use yii\widgets\ListView;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionCrud(){
        return $this->render("gestion");
    }
    
    public function actionConsulta1a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("count(*) cuenta"),
            'pagination'=>[
                'pageSize'=>1,
            ]
        ]);
        
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['cuenta'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT COUNT(*) FROM ciclista",
        ]);
        
    }
    public function actionConsulta1(){
        
        /*$numero = Yii::$app->db
                ->createCommand('select count(*)cuenta from ciclista')
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT COUNT(*)cuenta FROM ciclista',
            'totalCount'=>1,
            'pagination'=>[
                'pageSize' => 1,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['cuenta'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT COUNT(*) FROM ciclista",
        ]);
    }
    
    public function actionConsulta2a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("count(*) dorsal")
                    ->distinct()->where("nomequipo='Banesto'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT COUNT(*) FROM ciclista WHERE nomequipo = 'Banesto'",
        ]);
        
    }
    
    public function actionConsulta2(){
        //mediante DAO
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(distinct edad) FROM ciclista WHERE nomequipo = 'Artiach'")
                ->queryScalar();*/
        
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT COUNT(*)cuenta FROM ciclista WHERE nomequipo = 'Banesto'",
            //'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 1,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['cuenta'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT COUNT(*) FROM ciclista WHERE nomequipo = 'Banesto'",
        ]);
    }
    
    public function actionConsulta3a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                    ->select("dorsal")
                    ->where("nomequipo='Banesto' AND edad BETWEEN 25 AND 32"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Listar el dorsal de los ciclistas que son de Banesto y cuya edad está entre 25 y 32",
            "sql"=>"SELECT dorsal FROM ciclista WHERE nomequipo='Banesto' OR edad BETWEEN 25 AND 32",
        ]);
        
        /*echo ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => 'resultado1',
        ]);*/
        
    }
    
    public function actionConsulta3(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(DISTINCT edad) FROM ciclista WHERE nomequipo = 'Artiach' OR nomequipo = 'Amore Vita'")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT dorsal FROM ciclista WHERE nomequipo='Banesto' AND edad BETWEEN 25 AND 32",
            //'totalCount' => $numero,
            /*'pagination' =>[
                'pageSize'=>5,
            ]*/
        ]);
        
        return $this->render('resultado',[
            "resultados" => $dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que son de Banesto y cuya edad está entre 25 y 32",
            "sql"=>"SELECT dorsal FROM ciclista WHERE nomequipo='Banesto' AND edad BETWEEN 25 AND 32",
        ]);
            
        
        
    }
    
    public function actionConsulta4a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                    ->select("dorsal")
                    ->where("nomequipo='Banesto' OR edad BETWEEN 25 AND 32"),
                'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"Listar el dorsal de los ciclistas que son de Banesto o cuya edad está entre 25 y 32",
            "sql"=>"SELECT dorsal FROM ciclista WHERE nomequipo='Banesto' OR edad BETWEEN 25 AND 32",
        ]);
        
    }
    
    public function actionConsulta4(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM ciclista WHERE edad<24 OR edad>30")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT dorsal FROM ciclista WHERE nomequipo='Banesto' OR edad BETWEEN 25 AND 32",
            //'totalCount' =>$numero,
            'pagination'=>[
                'pageSize'=>5
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que son de Banesto o cuya edad está entre 25 y 32",
            "sql"=>"SELECT dorsal FROM ciclista WHERE nomequipo='Banesto' OR edad BETWEEN 25 AND 32",
        ]);
    }
    
    public function actionConsulta5a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                    ->select("nomequipo, avg(edad)edad")
                    ->distinct()
                    ->groupBy('nomequipo'),                 
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','edad'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT nomequipo, AVG(edad) FROM ciclista GROUP BY nomequipo",
        ]);
         
    }
    
    public function actionConsulta5(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM ciclista WHERE (edad BETWEEN 28 AND 32) AND (nomequipo = 'Banesto')")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT nomequipo, AVG(edad)edad_media FROM ciclista GROUP BY nomequipo",
            //'totalCount' =>$numero,
            'pagination'=>[
                'pageSize'=>5
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','edad_media'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT nomequipo, AVG(edad) FROM ciclista GROUP BY nomequipo",
        ]);
    }
    
    public function actionConsulta6a(){
    //por terminar    
        $dataProvider = new ActiveDataProvider([
           'query'  => Ciclista::find()
                ->select("nomequipo,count('dorsal')cuenta")
                ->distinct()
                ->groupBy('nomequipo'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','cuenta'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>"SELECT nomequipo, COUNT(*) FROM ciclista GROUP BY nomequipo",
        ]);
        
    }
    
    public function actionConsulta6(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(distinct nombre) FROM  ciclista WHERE CHAR_LENGTH(nombre)>8")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT nomequipo, COUNT(*)cuenta FROM ciclista GROUP BY nomequipo",
           //'totalCount' => $numero,
           'pagination'=>[
               'pageSize'=>5,
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','cuenta'],
            "titulo"=>"Consulta 6 con DAO",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>"SELECT nomequipo, COUNT(*) FROM ciclista GROUP BY nomequipo",
        ]);
        
    }
    
    public function actionConsulta7a(){
        
        $dataProvider = new ActiveDataProvider([
            'query'=> Puerto::find()
                ->select("count('nompuerto')cuenta")
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['cuenta'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT COUNT(*) FROM puerto",
            
        ]);
     
    }
    
    public function actionConsulta7(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM ciclista")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT COUNT(*)puertos FROM puerto",
           //'totalCount' => $numero,
           'pagination' => [
               'pageSize' =>1
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['puertos'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT COUNT(*) FROM puerto",
        ]);
        
    }
    
    public function actionConsulta8a(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()
                ->select("nompuerto")
                ->where("altura>2400")
                ->orWhere("altura between 1000 and 2000"),
            'pagination' => [
               'pageSize' =>5
           ]
                
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>"Listar el nombre de los puertos cuya altura esté entre 1000 y 2000 o que la altura sea mayor que 2400",
            "sql"=>"SELECT nompuerto FROM puerto WHERE altura BETWEEN 1000 AND 2000 OR altura>2400",
            
        ]);
     
    }
    
    public function actionConsulta8(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM lleva WHERE código = 'MGE'")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT nompuerto FROM puerto WHERE altura BETWEEN 1000 AND 2000 OR altura>2400",
           //'totalCount' => $numero,
           'pagination' => [
               'pageSize' =>5
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"Listar el nombre de los puertos cuya altura esté entre 1000 y 2000 o que la altura sea mayor que 2400",
            "sql"=>"SELECT nompuerto FROM puerto WHERE altura BETWEEN 1000 AND 2000 OR altura>2400",
        ]);
        
    }
    
    public function actionConsulta9a(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select('nomequipo')
                ->groupBy('nomequipo')
                ->having("count(*)>4")
                
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(*)>4",
            
        ]);
     
    }
    
    public function actionConsulta9(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM puerto")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(*)>4",
           //'totalCount' => $numero,
           'pagination' => [
               'pageSize' =>5
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(*)>4",
        ]);
        
    }
    
    public function actionConsulta10a(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select('nomequipo')
                ->where("edad>=28")
                ->andWhere("edad<=32")
                ->groupBy('nomequipo')
                ->having("count(*)>4"), 
                
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>"SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*)>4",
            
        ]);
     
    }
    
    public function actionConsulta10(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM puerto WHERE pendiente>8 OR altura BETWEEN 1800 AND 3000")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*)>4",
           //'totalCount' => $numero,
           'pagination' => [
               'pageSize' =>5
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>"SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*)>4",
        ]);
        
    }
    
    public function actionConsulta11a(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Etapa::find()
                ->select("dorsal, count(*)cuenta")
                ->groupBy('dorsal')
                
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','cuenta'],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT dorsal, COUNT(*) FROM etapa GROUP BY dorsal",
            
        ]);
     
    }
    
    public function actionConsulta11(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT dorsal, COUNT(*)cuenta FROM etapa GROUP BY dorsal",
           //'totalCount' => $numero,
           'pagination' => [
               'pageSize' =>5
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','cuenta'],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT dorsal, COUNT(*) FROM etapa GROUP BY dorsal",
        ]);
        
    }
    
    public function actionConsulta12a(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()
                ->select("count(distinct dorsal)cuenta")
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['cuenta'],
            "titulo"=>"Consulta 12 con Active Record",
            "enunciado"=>"Listar el número de ciclistas que hayan ganado algún puerto",
            "sql"=>"SELECT COUNT(DISTINCT dorsal) FROM puerto",
            
        ]);
     
    }
    
    public function actionConsulta12(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT COUNT(DISTINCT dorsal)cuenta FROM puerto",
           //'totalCount' => $numero,
           'pagination' => [
               'pageSize' =>1
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['cuenta'],
            "titulo"=>"Consulta 12 con DAO",
            "enunciado"=>"Listar el número de ciclistas que hayan ganado algún puerto",
            "sql"=>"SELECT COUNT(DISTINCT dorsal) FROM puerto",
        ]);
        
    }
    
    public function actionConsulta14a(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()
                ->select("avg(altura)media")
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['media'],
            "titulo"=>"Consulta 14 con Active Record",
            "enunciado"=>"Indicar la altura media de los puertos",
            "sql"=>"SELECT AVG(altura) FROM puerto",
            
        ]);
     
    }
    
    public function actionConsulta14(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT AVG(altura)media FROM puerto",
           //'totalCount' => $numero,
           'pagination' => [
               'pageSize' =>1
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['media'],
            "titulo"=>"Consulta 14 con DAO",
            "enunciado"=>"Indicar la altura media de los puertos",
            "sql"=>"SELECT AVG(altura) FROM puerto",
        ]);
        
    }
    
    public function actionConsulta18a(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Lleva::find()
                ->select("dorsal, código, count(*)cuenta")
                ->groupby("dorsal,código"),
            'pagination' => [
               'pageSize' =>5
           ]
        ]);
        
        return $this->render('resultado1', [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','código','cuenta'],
            "titulo"=>"Consulta 18 con Active Record",
            "enunciado"=>"Listar el dorsal del ciclista con el código de maillot y cuántas veces ese ciclista ha llevado ese maillot",
            "sql"=>"SELECT dorsal, código, COUNT(*) FROM lleva GROUP BY dorsal, código",
            
        ]);
        
     
    }
    
    public function actionConsulta18(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT dorsal, código, COUNT(*)cuenta FROM lleva GROUP BY dorsal, código",
           //'totalCount' => $numero,
           'pagination' => [
               'pageSize' =>5
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','código','cuenta'],
            "titulo"=>"Consulta 18 con DAO",
            "enunciado"=>"Listar el dorsal del ciclista con el código de maillot y cuántas veces ese ciclista ha llevado ese maillot",
            "sql"=>"SELECT dorsal, código, COUNT(*) FROM lleva GROUP BY dorsal, código",
        ]);
        
    }
    
}
