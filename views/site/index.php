<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas de Selección 3</h1>

        <p class="lead">Módulo 3 - Unidad 2</p>
    </div>

    <div class="body-content">

        <div class="row">
            <!--  
            botón de consulta
            -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1</h3>
                        <p>Listar las edades de todos los ciclistas de Banesto</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta1a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta1'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!--  
            fin botón de consulta
            -->
            <!-- inicio consulta 2 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 2</h3>
                        <p>Listar las edades de los ciclistas que son de Banesto o de Navigare</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta2a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta2'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 2 -->
            <!-- inicio consulta 3 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 3</h3>
                        <p>Listar el dorsal de los ciclistas que son de Banesto y cuya edad está entre 25 y 32</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta3a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta3'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 3 -->
            <!-- inicio consulta 4 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 4</h3>
                        <p>Listar el dorsal de los ciclistas que son de Banesto o cuya edad está entre 25 y 32</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta4a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta4'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 4 -->
            <!-- inicio consulta 5 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 5</h3>
                        <p>Listar la inicial del equipo de los ciclistas cuyo nombre comience por R</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta5a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta5'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 5 -->
            <!-- inicio consulta 6 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 6</h3>
                        <p>Listar el código de las etapas que su salida y llegada sea en la misma población</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta6a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta6'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 6 -->
            <!-- inicio consulta 7 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 7</h3>
                        <p>Listar el código de las etapas que su salida y llegada no sean en la misma población y que conozcamos el dorsal del ciclista que ha ganado</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta7a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta7'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 7 -->
            
            <!-- inicio consulta 8 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 8</h3>
                        <p>Listar el nombre de los puertos cuya altura esté entre 1000 y 2000 o que la altura sea mayor que 2400</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta8a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta8'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 8 -->
            
            <!-- inicio consulta 9 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 9</h3>
                        <p>Listar el dorsal de los ciclistas que hayan ganado los puertos cuya altura esté entre 1000 y 2000 o que la altura sea mayor que 2400</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta9a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta9'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 9 -->
            
            <!-- inicio consulta 10 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 10</h3>
                        <p>Listar el número de ciclistas que hayan ganado alguna etapa</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta10a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta10'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 10 -->
            
            <!-- inicio consulta 11 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 11</h3>
                        <p>Listar el número de etapas que tengan puerto</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta11a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta11'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 11 -->
            
            <!-- inicio consulta 12 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 12</h3>
                        <p>Listar el número de ciclistas que hayan ganado algún puerto</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta12a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta12'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 12 -->
            
            <!-- inicio consulta 13 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 13</h3>
                        <p>Listar el código de la etapa con el número de puertos que tiene</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta13a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta13'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 13 -->
            
            <!-- inicio consulta 14 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 14</h3>
                        <p>Indicar la altura media de los puertos</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta14a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta14'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 14 -->
            
            <!-- inicio consulta 15 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 15</h3>
                        <p>Indicar el código de etapa cuya altura media de sus puertos está por encima de 1500</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta15a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta15'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 15 -->
            
            <!-- inicio consulta 16 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 16</h3>
                        <p>Indicar el número de etapas que cumplen la condición anterior</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta16a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta16'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 16 -->
            
            <!-- inicio consulta 17 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 17</h3>
                        <p>Listar el dorsal del ciclista con el número de veces que ha llevado algún maillot</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta17a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta17'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 17 -->
            
            <!-- inicio consulta 18 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 18</h3>
                        <p>Listar el dorsal del ciclista con el código de maillot y cuántas veces ese ciclista ha llevado ese maillot</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta18a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta18'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 18 -->
            
            <!-- inicio consulta 19 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 19</h3>
                        <p>Listar el dorsal, el código de etapa, el ciclista y el número de maillots que ese ciclista ha llevado en cada etapa</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta12a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta12'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 19 -->
        </div>

    </div>
</div>
